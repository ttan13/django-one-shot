from django.shortcuts import render
from django.urls import reverse_lazy
from todos.models import TodoList, TodoItem
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView


# Create your views here.


class TodoListListView(ListView):
    model = TodoList
    template_name = "todos_list/list.html"
    # context_object_name = "todos_list"


class TodoListDetailView(DetailView):
    model = TodoList
    template_name = "todos_list/detail.html"
    # context_object_name = "todos_list_detail"


class TodoListCreateView(CreateView):
    model = TodoList
    template_name = "todos_list/create.html"
    fields = ["name"]
    # success_url = reverse_lazy("todos_list")

    def get_success_url(self):
        return reverse_lazy("todos_list_detail", args=[self.object.id])


class TodoListUpdateView(UpdateView):
    model = TodoList
    template_name = "todos_list/edit.html"
    fields = ["name"]
    # success_url = reverse_lazy("todos_list")

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.id])


class TodoListDeleteView(DeleteView):
    model = TodoList
    template_name = "todos_list/delete.html"
    success_url = reverse_lazy("todos_list")


class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = "item/create.html"
    fields = ["task", "due_date", "is_completed", "list"]
    success_url = reverse_lazy("todos_list")

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.list.id])


class TodoItemEditView(UpdateView):
    model = TodoItem
    template_name = "item/edit.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.list.id])

# class TodoItemListView(ListView):
#     model = TodoItem
#     template_name = "todos_item/list.html"

# class TodoItemDetailView(DetailView):
#     model = TodoItem
#     template_name = "todos_item/detail.html"
#     context_object_name = "todo_item_detail"

# class TodoItemDeleteView(DeleteView):
#     model = TodoItem
#     template_name = "todos_item/delete.html"