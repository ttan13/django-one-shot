from django.contrib import admin
from django.urls import path

from todos.views import (
    TodoItemCreateView,
    TodoItemEditView,
    # TodoItemListView,
    # TodoItemDetailView,
    # TodoItemDeleteView,

    TodoListCreateView,
    TodoListListView,
    TodoListDeleteView,
    TodoListDetailView,
    TodoListUpdateView,
    
)


urlpatterns = [
    path("", TodoListListView.as_view(), name="todos_list"),
    path("<int:pk>/", TodoListDetailView.as_view(), name="todos_list_detail"),
    path("create/", TodoListCreateView.as_view(), name="todos_list_create"),
    path("<int:pk>/edit/", TodoListUpdateView.as_view(), name="todos_list_edit"), 
    path("<int:pk>/delete/", TodoListDeleteView.as_view(), name="todos_list_delete"),
    path("items/create/", TodoItemCreateView.as_view(), name="todos_item_create"),  
    path("items/<int:pk>/edit/", TodoItemEditView.as_view(), name="todos_item_edit"),
    # path("items/<int:pk>/list/", TodoItemListView.as_view(), name="todos_item_list"),
    # path("items/<int:pk>/detail/", TodoItemDetailView.as_view(), name="todos_item_detail"),
    # path("items/<int:pk>/delete/", TodoItemDeleteView.as_view(), name="todos_item_delete"),
    ]
